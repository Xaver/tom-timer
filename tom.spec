Name: tom-timer
Version: 0.0.1
Release: 1
Summary: "tom-timer is a simple timer service"
License: MIT

%description
Provides a server and client to create timers over dbus
and execute files on timer completion

%build
make

%install
mkdir -p %{buildroot}/usr/bin/
install -m 755 _build/tom %{buildroot}/usr/bin/tom
install -m 755 _build/tomctl %{buildroot}/usr/bin/tomctl

%files
/usr/bin/tom
/usr/bin/tomctl
