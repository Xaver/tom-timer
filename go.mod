module codeberg.org/Xaver/tom-timer

go 1.19

require (
	codeberg.org/Xaver/syncmap v0.0.0-20230113141421-6cbbb21236c4
	github.com/godbus/dbus/v5 v5.1.0
	github.com/spf13/cobra v1.6.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
