package common

import (
	"github.com/godbus/dbus/v5/introspect"
)

const (
	BusPath            = "/org/codeberg/xaver/tom_timer"
	BusInterface       = "org.codeberg.xaver.tom_timer"
	MethodStartTimer   = BusInterface + ".StartTimer"
	MethodCancelTimer  = BusInterface + ".CancelTimer"
	MethodActiveTimers = BusInterface + ".ActiveTimers"
)

const IntrospectStr = `
<node>
	<interface name="` + BusInterface + `">
		<method name="StartTimer">
			<arg name="seconds" direction="in" type="u"/>
			<arg name="name" direction="in" type="s"/>
			<arg name="exec" direction="in" type="s"/>

			<arg name="timer_id" direction="out" type="t"/>
		</method>
		<method name="CancelTimer">
			<arg name="timer_id" direction="in" type="t"/>
		</method>

		<method name="ActiveTimers">
			<arg name="timers" direction="out" type="a(tsss)"/>
		</method>

	</interface>
	` + introspect.IntrospectDataString + `
</node>
`

type TimerInfo struct {
	Id         uint64 `json:"id"`
	Name       string `json:"name"`
	Exec       string `json:"exec"`
	Timeout    string `json:"timeout"`
	ExpireTime string `json:"expireTime"`
}
