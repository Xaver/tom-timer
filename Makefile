GOOS= $(shell go env GOOS)
GOARCH= $(shell go env GOARCH)
VERSION= $(shell git describe --dirty --broken --always)
VERSION := $(VERSION)-$(GOOS)-$(GOARCH)

LDFLAGS =-ldflags "-X 'main.version=$(VERSION)'"

GO_FLAGS= $(LDFLAGS)

build: clean
	go build $(GO_FLAGS) -o _build/tom cmd/server/tom.go
	go build $(GO_FLAGS) -o _build/tomctl cmd/client/tomctl.go

rpm:
	rpmbuild --build-in-place -ba tom.spec

clean:
	rm -rf _build/*

.PHONY: build
