# tom-timer

All that tom does is wait for a timer to expire and then execute a file


I built this for myself to have timer based notifications.

# Getting started
You can either build a rpm by running `make rpm` or just do a plain go build with `make`

In both cases you will have two binaries:
* `tom` - the server
* `tomctl` - a cli tool to control tom

You can create a new timer by running `tomctl create` for example `tomctl create 30s "$HOME/.config/ontimer.sh"`. This would create a 30 seconds timer which will execute `$HOME/.config/ontimer.sh` on completion.


## dbus
tom is controlled via dbus, this means that you don't have to use `tomctl` to control tom. You can build your own clients. The dbus interface is defined in `pkg/common/common.go`