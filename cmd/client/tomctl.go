package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	cmn "codeberg.org/Xaver/tom-timer/pkg/common"
	"github.com/godbus/dbus/v5"

	"github.com/spf13/cobra"
)

var errLog = log.New(os.Stderr, "ERROR ", log.LstdFlags)

var version = "UNDEFINED"

func main() {

	conn, err := dbus.SessionBus()
	if err != nil {
		errLog.Fatal("Unable to connect to the session bus", err)
	}

	obj := conn.Object(cmn.BusInterface, dbus.ObjectPath(cmn.BusPath))

	rootCmd := &cobra.Command{}

	rootCmd.AddCommand(&cobra.Command{
		Use:   "version",
		Short: "Show Version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(version)
			os.Exit(0)
		},
	})

	createCmd := &cobra.Command{
		Use:     "create timeout file-to-execute [name]",
		Short:   "Create a new timer",
		Long:    createTimerDocs,
		Example: `create 30s "$HOME/ontimer.sh" "my optional name"`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				errLog.Fatal("expected at least 2 arguments but got", len(args))
			}

			duration, err := time.ParseDuration(args[0])
			if err != nil {
				errLog.Fatal("unable to parse timeout", err)
			}

			exec := args[1]
			name := ""
			if len(args) >= 3 {
				name = args[2]
			}

			call := obj.CallWithContext(context.Background(), cmn.MethodStartTimer, 0,
				uint64(duration.Seconds()),
				name,
				exec,
			)

			id := uint64(0)
			if err := call.Store(&id); err != nil {
				errLog.Fatal("Failed to create Timer:", err)
			}
			fmt.Println(id)
		},
	}
	rootCmd.AddCommand(createCmd)

	cancelCmd := &cobra.Command{
		Use:     "cancel id",
		Short:   "Cancel an active timer",
		Example: "cancel 3",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				errLog.Fatal("expected 1 argument but got", len(args))
			}

			id, err := strconv.Atoi(args[0])
			if err != nil {
				errLog.Fatal("Unable to parse tiemr id", err)
			}
			if err := obj.CallWithContext(context.Background(), cmn.MethodCancelTimer, 0,
				uint64(id),
			).Store(); err != nil {
				errLog.Fatal("Failed to cancel timer:", err)
			}
		},
	}
	rootCmd.AddCommand(cancelCmd)

	listCmd := &cobra.Command{
		Use:   "list",
		Short: "List active timers",
		Run: func(cmd *cobra.Command, args []string) {

			activeTimers := []cmn.TimerInfo{}
			if err := obj.CallWithContext(context.Background(), cmn.MethodActiveTimers, 0).Store(&activeTimers); err != nil {
				errLog.Fatal("Failed to list active timers:", err)
			}

			out, err := json.MarshalIndent(activeTimers, "", "  ")
			if err != nil {
				errLog.Fatal("json marshal failed", err)
			}
			fmt.Println(string(out))
		},
	}
	rootCmd.AddCommand(listCmd)

	rootCmd.Execute()

}

const createTimerDocs = `
create a timer with a given timeout and a file to execute on completion.
The function will print the id of the created timer to stdout if succesful.

You can optionally supply a name for the timer.

Please note that the provided file will be executed in the context of the server!

!!!IF THE SERVER RUNS AS ROOT THE FILE IS ALSO EXECUTED AS ROOT!!!

The executed file receives the timer id and name as arguments

Example File:

#!/bin/sh

# send name of timer as a notification
notify-send "${@: 2}"

`
