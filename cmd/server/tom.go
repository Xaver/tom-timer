package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"os/user"
	"sync/atomic"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"

	"codeberg.org/Xaver/syncmap"
	cmn "codeberg.org/Xaver/tom-timer/pkg/common"
)

var errLog = log.New(os.Stderr, "ERROR ", log.LstdFlags)
var infoLog = log.New(os.Stdout, "INFO ", log.LstdFlags)
var warnLog = log.New(os.Stdout, "WARN ", log.LstdFlags)

var version = "UNDEFINED"

func main() {

	if len(os.Args) > 1 {
		if os.Args[1] == "--version" || os.Args[1] == "version" {
			fmt.Println(version)
			os.Exit(0)
		}
	}

	if user, err := user.Current(); err == nil {
		if user.Uid == "0" {
			warnLog.Println("You should not run this server as root!")
		}
	}

	conn, err := dbus.SessionBus()
	if err != nil {
		errLog.Fatal("Unable to connect to the session bus", err)
	}
	defer conn.Close()

	t := &Timers{
		activeTimers: syncmap.New[uint64, Timer](),
	}

	if err := conn.Export(t, cmn.BusPath, cmn.BusInterface); err != nil {
		errLog.Fatal("Unable to export timers interface", err)
	}

	if err := conn.Export(introspect.Introspectable(cmn.IntrospectStr), cmn.BusPath, "org.freedesktop.DBus.Introspectable"); err != nil {
		errLog.Fatal("Unable to export introspectable interface", err)
	}

	reply, err := conn.RequestName(cmn.BusInterface, dbus.NameFlagDoNotQueue)
	if err != nil {
		errLog.Fatal("RequestName failed", err)
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		errLog.Fatal("Name already taken - is the server already running?")
	}

	end := make(chan (os.Signal), 1)
	signal.Notify(end, os.Interrupt, os.Kill)
	infoLog.Println("Ready")
	<-end
}

type Timer struct {
	cmn.TimerInfo
	cancel context.CancelFunc
}

type Timers struct {
	nextTimerId  atomic.Uint64
	activeTimers syncmap.Map[uint64, Timer]
}

func (t *Timers) CancelTimer(id uint64) *dbus.Error {
	t.activeTimers.WithLock(func(lm syncmap.LockedMap[uint64, Timer]) {
		if t, ok := lm.Get(id); ok {
			t.cancel()
			lm.Delete(id)
		}
	})
	return nil
}

func (t Timers) ActiveTimers() ([]cmn.TimerInfo, *dbus.Error) {
	timers := make([]cmn.TimerInfo, 0, t.activeTimers.Len())
	t.activeTimers.WithLock(func(lm syncmap.LockedMap[uint64, Timer]) {
		ids := lm.Keys()
		for _, id := range ids {
			timer, _ := lm.Get(id)
			timers = append(timers, timer.TimerInfo)
		}
	})
	return timers, nil
}

func (t *Timers) StartTimer(seconds uint, name string, execPath string) (uint64, *dbus.Error) {

	id := t.nextTimerId.Add(1)

	go func() {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		duration := time.Second * time.Duration(seconds)

		expireTime := time.Now().Add(duration)

		timer := Timer{
			TimerInfo: cmn.TimerInfo{
				Id:         id,
				Name:       name,
				Exec:       execPath,
				ExpireTime: expireTime.String(),
				Timeout:    duration.String(),
			},
			cancel: cancel,
		}

		t.activeTimers.Set(id, timer)
		defer t.activeTimers.Delete(id)

		completed := time.NewTimer(duration)
		defer completed.Stop()

		select {
		case <-completed.C:
			if execPath != "" {
				go func() {
					execCtx, cancel := context.WithTimeout(context.Background(), time.Second*10) // FIXME: configurable timeout
					defer cancel()
					if err := exec.CommandContext(execCtx, execPath, fmt.Sprint(id), name).Run(); err != nil {
						errLog.Printf("exec failed (timer id: %d name: '%s') - %s", id, name, err.Error())
					}
				}()
			}

		case <-ctx.Done():
			return
		}
	}()
	return id, nil
}
